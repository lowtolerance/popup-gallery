const {serveDir, serveAssetDir} = require('../../scripts/utils/path')
module.exports = {
  '/': serveDir('.'),
  '/assets/': serveAssetDir()
}
