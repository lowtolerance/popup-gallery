const urlEncodedParser = require('./urlEncodedParser')
const postConfig = require('./postConfig')

module.exports = {
  '/submit-config': [
    urlEncodedParser,
    postConfig,
    (req, res) => res.redirect('/config/' + res.locals.message)
  ]
}
