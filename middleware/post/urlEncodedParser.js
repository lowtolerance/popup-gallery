const bodyParser = require('body-parser')
bodyParser.json()
module.exports = bodyParser.urlencoded({ extended: false })
