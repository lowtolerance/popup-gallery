const {writeConfig} = require('../../scripts/utils/misc')

module.exports = (req, res, next) => {
  res.locals.message = (writeConfig(req.body))
    ? 'saveSuccessful'
    : 'saveError'
  next()
}
