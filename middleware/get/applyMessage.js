const messages = require('./messages.json')

module.exports = (req, res, next) => {
  res.locals.message = messages[req.params.message]
  next()
}
