const {getConfig} = require('../../scripts/utils/misc')
module.exports = (req, res) =>
  res.render('config', getConfig())
