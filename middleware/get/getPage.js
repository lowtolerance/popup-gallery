const {_array: {getSlice}, _misc: {getImageFiles}} = require('../../scripts/utils')

module.exports = (pageNumber = 1, pageLength = 15) => {
  let {images, thumbs} = getImageFiles()
  return {
    pageNumber,
    images: getSlice(images, pageNumber, pageLength),
    thumbs: getSlice(thumbs, pageNumber, pageLength),
    totalPages: Math.ceil(images.length / pageLength)
  }
}
