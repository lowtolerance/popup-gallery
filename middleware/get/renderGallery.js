const {getConfig} = require('../../scripts/utils/misc')
const getPage = require('./getPage')

module.exports = (req, res) => res.render(
  'gallery', getPage(req.params.page, getConfig('pageLength'))
)
