const applyPrivateMode = require('./applyPrivateMode')
const applyMessage = require('./applyMessage')
const renderGallery = require('./renderGallery')
const renderConfig = require('./renderConfig')

module.exports = {
  '/': [
    applyPrivateMode,
    renderGallery
  ],
  '/page/:page': [
    applyPrivateMode,
    renderGallery
  ],
  '/config': renderConfig,
  '/config/:message': [
    applyMessage,
    renderConfig
  ]
}
