const {getConfig} = require('../../scripts/utils/misc')

module.exports = (req, res, next) => {
  res.locals.privateMode = getConfig('privateMode')
  next()
}
