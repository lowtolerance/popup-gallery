#!/usr/bin/env node
const {_fn: {initiateInSequence}} = require('./scripts/utils')
const cleanupOnExit = require('./scripts/cleanupOnExit')

initiateInSequence(
  require('./scripts/getThumbnails'),
  require('./scripts/startServer')
)
cleanupOnExit()
