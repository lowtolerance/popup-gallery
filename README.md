Popup Gallery
=============

The idea here is really simple, just run `popup-gallery` at the command line in whatever folder you want to view and visit http://localhost:3000 in your browser to browse the images in that folder.

Installation
------------

`npm install -g popup-gallery`

Configuration
-------

Configuration is stored in the root application directory in the file `config.json`, but a front-end configuration tool is provided at http://localhost:3000/config. 

So far, there are only three configuration options:

* Privacy Mode - if enabled, gallery will load as a blank page. Enter the "konami code" to unlock
* Auto-open in browser - if enabled, app will launch a browser tab for itself automatically (requires restart)
* Port - set the port number for the server to listen on. (requires restart)
* Images Per Page - set the number of images to display on each page
* Cleanup On Exit - delete thumbnails directory on exit (requires restart)

To-do
----

* CSS could obviously use a lot of work.
* Allow gallery to be viewed as thumbnails are being generating, pushing them to page as they are completed.
* Infinite scrolling?
* User-configurable color scheme, themeing
* Slideshow mode