(function () {
  let konami = ''
  const check = (ev) => {
    konami += ev.keyCode
    if (konami === '3838404037393739666513') {
      document.body.removeAttribute('style')
      window.Macy({container: '.grid', waitForImages: true})
      document.removeEventListener('keydown', check)
    }
  }
  document.addEventListener('keydown', check)
})()
