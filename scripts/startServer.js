const {_path: {dirName}, _misc: {getConfig}} = require('./utils')
const useBundle = require('../middleware/use')
const getBundle = require('../middleware/get')
const postBundle = require('../middleware/post')
const expressSetup = require('./expressSetup')
const open = require('opn')

module.exports = () => {
  let {port} = getConfig()
  console.log(`Starting server at http://localhost:${port}`)
  expressSetup
    .set({'views': dirName('views')})
    .use(useBundle)
    .get(getBundle)
    .post(postBundle)
    .listen(port)
  if (getConfig().autoOpen) {
    console.log(`Opening http://localhost:${port} in browser`)
    open(`http://localhost:${port}`)
  }
}
