const {byImage} = require('./utils/filters')
const sharp = require('sharp')
const fs = require('fs')

const integerPercentage = (val, max) => Math.round(val * (100 / max))
let images = fs.readdirSync('.').filter(byImage)
const makeThumbs = async () => {
  let imagesProcessed = 0
  let thumbsWritten = 0
  await Promise.all(images.map(async (image) => {
    thumbsWritten++
    process.stdout.write(`Generating thumbnails: ${integerPercentage(thumbsWritten, images.length)}%\r`)
    return {
      name: image,
      buffer: await sharp(image).resize(400).toBuffer()
    }
  }))
    .then(values => {
      values.forEach((value, index) => {
        fs.writeFileSync('./thumbs/th-' + value.name, value.buffer)
        imagesProcessed++
        process.stdout.write((imagesProcessed !== images.length)
          ? `Writing thumbnails to disk: ${integerPercentage(index, images.length)}%\r`
          : 'Writing thumbsnails to disk: 100%\nDone!\n')
      })
    })
    .catch(error => console.log(error))
}

module.exports = async (fn) =>
  (!fs.existsSync('./thumbs'))
    ? (fs.mkdirSync('./thumbs'),
      makeThumbs())
    : (images.length !== fs.readdirSync('./thumbs').length)
      ? makeThumbs()
      : console.log('Thumbnails found')
