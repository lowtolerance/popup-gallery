const {getConfig} = require('./utils/misc')
const nodeCleanup = require('node-cleanup')
const rimraf = require('rimraf')

module.exports = () => (getConfig('cleanupOnExit')) &&
  nodeCleanup(() => rimraf.sync('./thumbs'))
