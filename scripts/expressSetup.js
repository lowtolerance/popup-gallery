module.exports = (() => {
  const app = require('express')()

  const methods = ['set', 'use', 'get', 'post']
  methods.forEach((method, middleware) => {
    const applyMiddleware = (middleware, method) => {
      Object.entries(middleware).forEach(([key, value]) => {
        app[method](key, value)
      })
      return this
    }
    this[method] = (middleware) =>
      applyMiddleware.call(this, middleware, method)
    return this
  })

  let engineCalled = false
  this.engine = (engine = 'pug') => {
    if (!engineCalled) {
      app.set('view engine', engine)
      engineCalled = true
    }
    return this
  }

  this.listen = (port = 3000, cb) => {
    if (cb == null) {
      cb = () => console.log(`Listening on ${port}`)
    }
    this.engine()
    app.listen(port, cb())
    return this
  }
  return this
})()
