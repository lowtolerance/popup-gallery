const fs = require('fs')
const path = require('path')
const _filters = require('./filters')

const _misc = (() => {
  const getConfig = (key) => {
    let config = JSON.parse(fs.readFileSync(path.join(__dirname, '../../config.json')))
    let value = (key)
      ? config[key]
      : config
    return value
  }

  const writeConfig = obj => {
    Object.keys(getConfig()).forEach(key => {
      if (obj[key] === 'on') obj[key] = true
      if (obj[key] === undefined) obj[key] = false
    })
    try {
      fs.writeFileSync(
        path.join(__dirname, '../../config.json'),
        JSON.stringify(obj)
      )
    } catch (error) {
      console.log(error)
      return false
    }
    return true
  }

  const getImageFiles = () => {
    const {byImage} = _filters
    return {
      images: fs.readdirSync('.').filter(byImage),
      thumbs: fs.readdirSync('./thumbs')
    }
  }
  return {getConfig, writeConfig, getImageFiles}
})()

module.exports = _misc
