const _array = {
  getSlice: (arr, sliceIndex, sliceLength = 15) =>
    arr.slice(sliceLength * (sliceIndex - 1), sliceLength * sliceIndex)
}

module.exports = _array
