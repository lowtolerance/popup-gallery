const _path = require('./path')
const _array = require('./array')
const _fn = require('./fn')
const _filters = require('./filters')
const _misc = require('./misc')

module.exports = {
  _path, _array, _fn, _misc, _filters
}
