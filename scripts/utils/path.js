const path = require('path')
const express = require('express')

const _path = (() => {
  const assetDir = loc => 'public/' + loc
  const dirName = loc => path.join(__dirname, '../../' + loc)
  const serveDir = loc => express.static(loc)
  const serveAssetDir = loc => serveDir(path.join(__dirname, '../../public/'))
  const serveAsset = loc => serveDir(dirName(assetDir(loc)))
  return { assetDir, dirName, serveDir, serveAsset, serveAssetDir }
})()

module.exports = _path
