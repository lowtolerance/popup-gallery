const _fn = {
  initiateInSequence: (first, second) =>
    (async () => {
      await (async () => {
        await first()
      })()
      second()
    })()
}

module.exports = _fn
