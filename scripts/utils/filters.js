const path = require('path')

const _filters = (() => {
  let images = ['.jpg', '.png', '.gif', '.bmp', '.jpeg']
  const byExtension = (arr, item) =>
    arr.includes(path.extname(item))
  const byImage = byExtension.bind(null, images)
  return {byImage}
})()

module.exports = _filters
